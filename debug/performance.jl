
using MuSink

function get_package(sym)
    try getfield(Main, sym)
    catch
        nothing
    end
end

function benchmark_step(; n = 100, m = 100, reach = 100, logdomain = true, atype = :array32, separable = true, sync = () -> nothing)
    # Create workspace and precompile
    cost = separable ? Lp : LpNotSep
    ws = MuSink.testspace(; n, m, reach, logdomain, atype, cost, ntargets = 5)
    step!(ws)

    # Time the actual run
    dt = @elapsed begin
        step!(ws)
        sync()
    end

    # Pretty print the result
    ld = logdomain ? "logdomain" : "expdomain"
    sep = separable ? "separable" : "nonseparable"

    if atype in [:array32, :array64]
        LV = get_package(:LoopVectorization)
        if LV != nothing
            t = "($(Threads.nthreads()), avx)"
        else
            t = "($(Threads.nthreads()))"
        end
    else
        t = ""
    end
    println("$(n)x$(m)x$(reach) $ld $sep $atype$t: $dt")
end

function benchmark_step_atypes(; kwargs...)
    CUDA = get_package(:CUDA)
    oneAPI = get_package(:oneAPI)
    Metal = get_package(:Metal)

    args = [(:array32, () -> nothing), (:array64, () -> nothing)]
    if CUDA != nothing
        push!(args, (:cuda32, CUDA.synchronize))
        push!(args, (:cuda64, CUDA.synchronize))
    end
    if oneAPI != nothing
        push!(args, (:one32, oneAPI.synchronize))
        push!(args, (:one64, oneAPI.synchronize))
    end
    if Metal != nothing
        push!(args, (:metal32, Metal.synchronize))
    end

    for (atype, sync) in args
        benchmark_step(; atype, sync, kwargs...)
    end
end

benchmark_step_atypes(n = 100, m = 100, reach = 100, separable = true)
println()
benchmark_step_atypes(n = 300, m = 300, reach = 300, separable = true)
println()
benchmark_step_atypes(n = 500, m = 500, reach = 50, separable = true)
println()
benchmark_step_atypes(n = 100, m = 100, reach = 100, separable = false)
println()
benchmark_step_atypes(n = 300, m = 300, reach = 30, separable = false)

