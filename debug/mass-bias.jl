
using Revise
using Statistics, LinearAlgebra, Random

using CUDA

#using .MuSink
using MuSink
import MuSink.Tree

c0 = Tree.Root(0)
c1 = Tree.new_child!(c0, 1)
c2 = Tree.new_child!(c0, 2)
c3 = Tree.new_child!(c2, 3)
c4 = Tree.new_child!(c2, 4)

n = 64
m = n

Random.seed!(0)

img0 = rand(n, n, 1)
img0[1:div(n, 3), 1:div(m, 3), :] .= 1
img0 ./= sum(img0)
#img0 .*= n^2

img1 = rand(n, n, 1)
img1[div(n, 3):2div(n, 3), div(m, 3):2div(m, 3), :] .= 1
img1 ./= sum(img1)
#img1 .*= n^2

img2 = rand(n, n, 1)
img2[2div(n, 3):3div(n, 3), 2div(m, 3):3div(m, 3), :] .= 1
img2 ./= sum(img2)
#img2 .*= n^2

img3 = rand(n, n, 1)
img3[2div(n, 3):3div(n, 3), 2div(m, 3):3div(m, 3), :] .= 1
img3[div(n, 3):2div(n, 3), div(m, 3):2div(m, 3), :] .= 1
img3 ./= sum(img3)
#img3 .*= n^2

img4 = rand(n, n, 1)
img4[1:3div(n, 3), 1:3div(m, 3), :] .= 1
img4[div(n, 3):2div(n, 3), div(m, 3):2div(m, 3), :] .= 1
img4 ./= sum(img4)
#img4 .*= n^2

marginals = Dict(
  c0 => img0 * 80.0,
  c1 => img1 * 2.0,
  c2 => img2 * 2.0,
  c3 => img3 * 1.0,
  c4 => img4 * 0.1
)

eps = 0.1
rho = 30
reach = n - 20 #div(n, 20) - 1
logdomain = true
symmetric = false
T = CuArray{Float32}

# references = MuSink.references_counting(marginals, 1.0)
references = MuSink.references(marginals, 2.0)

cost = MuSink.Lp(n, m, max = 50000.0)
penalty = MuSink.TotalVariation()
problem = MuSink.Problem(marginals; cost, penalty, references)
ws = MuSink.Workspace(T, problem; eps, rho, reach, logdomain, symmetric)

MuSink.sync!(ws)
entropic_mass = MuSink.mass(ws)
@show entropic_mass

#MuSink.adapt!(ws, eps = eps)
 for i in 1:1000
   MuSink.step!(ws)

   errval = maximum(keys(marginals)) do node
     marg = convert(typeof(ws.buffer), marginals[node])
     mean(abs, MuSink.marginal(ws, node) .- marg) / mean(marg)
   end
  @show errval

   println()
 end

@show MuSink.mass(ws)
