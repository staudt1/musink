
using Revise, MuSink
import MuSink: Problem, Workspace, Tree
import .Tree: Sequence, Star

# ws1 = MuSink.testspace(eps = 0.01, logdomain = true)
# ws2 = MuSink.Workspace( Array{Float64}, ws1.problem; eps = 1, ws1.rho, logdomain = true)

# MuSink.adapt_eps!(ws2, 0.01)
# MuSink.adapt_weight!(ws2, 1, 2, 0.1)

# MuSink.steps!(ws1, 100)
# MuSink.steps!(ws2, 100)

###################

left = zeros(1, 3) 
right = zeros(1, 3)
middle = ones(1, 3)

left[:,1] .+= 1
left[:, end] .+= 1

right[:, 2] .+= 1
right[:, end] .+= 1

eps = 0.01
rho = Inf
logdomain = true
cost = MuSink.Lp(1, 3, p = 2)

stepmode = :alternating
steps = 100

println("ROOT: 1")
root = Sequence(3)
p = Problem(root, [left, middle, right]; cost)
w1 = Workspace(p; eps, rho, logdomain)

MuSink.adapt_rho!(w1, 2, 0.0)
for i in 1:steps
  MuSink.step!(w1; stepmode)
  @show MuSink.step_impact_plan(w1)
end

@show MuSink.marginal(w1, 1)
@show MuSink.marginal(w1, 2)
@show MuSink.marginal(w1, 3)
println()

println("ROOT: 2")
root = Tree.reroot(root, 2)
p = Problem(root, [left, middle, right]; cost)
w2 = Workspace(p; eps, rho, logdomain)

MuSink.adapt_rho!(w2, 2, 0.0)
for i in 1:steps
  MuSink.step!(w2; stepmode)
  @show MuSink.step_impact_plan(w2)
end

@show MuSink.marginal(w2, 1)
@show MuSink.marginal(w2, 2)
@show MuSink.marginal(w2, 3)
println()

println("ROOT: 3")
root = Tree.reroot(root, 3)
p = Problem(root, [left, middle, right]; cost)
w3 = Workspace(p; eps, rho, logdomain)

MuSink.adapt_rho!(w3, 2, 0.0)
for i in 1:steps
  MuSink.step!(w3; stepmode)
  @show MuSink.step_impact_plan(w3)
end

@show MuSink.marginal(w3, 1)
@show MuSink.marginal(w3, 2)
@show MuSink.marginal(w3, 3)


###################

# topleft = zeros(3, 3)
# # topleft .+= 1e-10
# topleft[1,1] = 1
# topleft ./= sum(topleft)

# bottomleft = zeros(3, 3)
# # bottomleft .+= 1e-10
# bottomleft[end,1] = 1
# bottomleft ./= sum(bottomleft)

# topright = zeros(3, 3)
# # topright .+= 1e-10
# topright[1,end] = 1
# topright ./= sum(topright)

# bottomright = zeros(3, 3)
# # bottomright .+= 1e-10
# bottomright[end,end] = 1
# bottomright ./= sum(bottomright)

# middle1 = zeros(3, 3)
# # middle1 .+= 1e-10
# middle1[2, 3] = 1
# middle1 ./= sum(middle1)

# targets = [middle1, topleft, topright, bottomleft, bottomright]

# root = Star(5)
# node = Tree.descendant(root, 2)
# root = Tree.reroot(node)

# p = Problem(root, targets)#; references = targets)
# ws = Workspace(p, eps = 0.0001, rho = 10, logdomain = true)

# # MuSink.adapt_weight!(ws, 1, 2, 0.1)

# MuSink.adapt_rho!(ws, 1, 0.0)

# # MuSink.adapt_reach!(ws, 100)
# # MuSink.adapt_rho!(ws, 3, 0.0)

# @show MuSink.mass(ws)
# @show MuSink.cost(ws)
# println()

# MuSink.steps!(ws, 10)
# @show MuSink.mass(ws)
# @show MuSink.cost(ws)
# println()

# MuSink.steps!(ws, 100)
# @show MuSink.mass(ws)
# @show MuSink.cost(ws)
# println()

# MuSink.steps!(ws, 1000)
# @show MuSink.mass(ws)
# @show MuSink.cost(ws)
# println()

# MuSink.steps!(ws, 10000)
# @show MuSink.mass(ws)
# @show MuSink.cost(ws)
# println()

# MuSink.marginal(ws, 1)