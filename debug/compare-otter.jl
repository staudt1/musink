
using Random
using CUDA

include("../src/MuSink.jl")
using .MuSink

root = MuSink.Tree.Root(1)
child_1 = MuSink.Tree.new_child!(root, 2)

@show MuSink.Tree.descendants(root)
@show MuSink.Tree.descendants(root, false)
@show MuSink.Tree.neighbors(child_1)

Random.seed!(1)

n = 64

marg1 = rand(n, n, 1)
marg2 = rand(n, n, 1)

marg1[1:20, 5:30] .= 1
marg2[1:20, 5:30] .= 1e-10

marginals = Dict(
    root => marg1 / sum(marg1)
  , child_1 => marg2 / sum(marg2)
)

eps = 0.01
rho = Inf
logdomain = false
symmetric = false

cost = MuSink.LpSep(; reach = 63)
p = MuSink.Problem(marginals; cost, divergence = MuSink.TotalVariation())
w = MuSink.Workspace(CuArray{Float32}, p; logdomain, symmetric, eps, rho)

MuSink.step!(w)

CUDA.@time for i in 1:100
  MuSink.step!(w)
  impact =  MuSink.step_impact_plan(w)
  @show impact
end


p1 = MuSink.marginal(w, root)
p2 = MuSink.marginal(w, child_1)

a = reshape(p.marginals[root], n*n)
b = reshape(p.marginals[child_1], n*n)

display(p.marginals[root][1:3, 1:3])
display(p1[1:3, 1:3])

cmat = MuSink.matrix(cost, n, n)

using Otter

println("Otter:")
res = @time Otter.sinkhorn_scale(a, b, cmat, reg = eps, tol = 1e-3, atype = "gpu")
res = @time Otter.sinkhorn_scale(a, b, cmat, reg = eps, tol = 1e-3, atype = "gpu")
otter_cost = Otter.cost(res)

println("MuSink:")
musink_cost = @time MuSink.cost(w)[1]
musink_cost = @time MuSink.cost(w)[1]

@show otter_cost
@show musink_cost

otter_plan = reshape(Otter.plan(res), n, n, n, n)
musink_plan = MuSink.dense(w, root, child_1)