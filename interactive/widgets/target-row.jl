
"""
Widget that visualizes the targets of the problem in a row.

### Interaction context
Requires the observables
  * :problem
  * :area
  * :visible_nodes
  * :show_targets
"""
struct TargetRow <: Widget end

function TargetRow(box_layout, ctx :: Context; title = "Target")
  @debug "TargetRow: creating widget"
  context_require(ctx, :problem, :visible_nodes, :show_targets)

  problem = ctx[:problem]
  selection = ctx[:visible_nodes]
  show = ctx[:show_targets]

  blocks = []
  blocks_at = []
  observers = []

  lift(problem) do problem
    @debug "TargetRow: drawing contents"

    foreach(block_delete!, blocks)
    foreach(block_delete!, reduce(vcat, blocks_at, init = []))
    foreach(off, observers)

    empty!(blocks)
    empty!(blocks_at)
    empty!(observers)

    layout = boxed_layout_horizontal(box_layout, title; blocks)
    layout_row = layout[1,1] = GridLayout(default_colgap = 10)

    n, m, _ = size(problem)
    mi, ma = Observable(Inf), Observable(-Inf)

    images = [Observable(zeros(n, m)) for _ in 1:length(problem)]

    for index in 1:length(problem)

      color = (Makie.COLOR_ACCENT_DIMMED[], 0.2)
      box = Box(layout_row[1, index]; strokewidth = 1, color, strokecolor = color)

      ax = Axis(layout_row[1, index]; aspect = DataAspect(), tellheight = false)

      img = MuSink.target(problem, index)
      img = dropdims(img, dims = 3)
      image!(ax, rotr90(img), colorrange = @lift ($mi, $ma))

      hidedecorations!(ax)
      deregister_interactions!(ax)
      register_multipointer!(ax, ctx)

      mi[] = min(mi[], minimum(img))
      ma[] = max(ma[], maximum(img))

      obs = on(ctx[:area], update = true) do area
        start_i, stop_i, start_j, stop_j = area
        ax.limits[] = ((start_j-1, stop_j), (start_i-1, stop_i))
      end

      push!(blocks_at, [])
      push!(blocks_at[index], box, ax)
      push!(observers, obs)
    end

    # Set the height of the colorbar to the height of the first
    # axis that is currently in the visible-nodes selection
    # idx = lift(show, selection) do _, sel
    #   index = first(sel)
    #   findfirst(x -> x isa Axis, blocks_at[index])
    #   area = blocks_at[index][idx].scene.px_area[]
    # end
    # height = lift(show, selection) do _, sel
    #   index = first(sel)
    #   idx = findfirst(x -> x isa Axis, blocks_at[index])
    #   area = blocks_at[index][idx].scene.px_area[]
    #   area.widths[2]
    # end

    cb =
      Colorbar(
        layout_row[1,length(problem)+1],
        colorrange = @lift(($mi, $ma)),
        colormap = [:white, :black],
        width = 10,
        tellheight = false,
        ticks = @lift([$mi, ($ma + $mi)/2, $ma]),
        tickformat = "{:.1e}",
        ticklabelspace = 50
      )

    push!(blocks, cb)

    # Show only selected parts
    obs_show = onany(show, selection) do show, sel
      # show visible (selected) nodes
      if show
        rowsize!(box_layout, 1, Auto())
        foreach(block_unhide!, blocks)

        for index in 1:length(problem)
          if index in sel
            colsize!(layout_row, index, Auto())
            colgap!(layout_row, index, 10)
            foreach(block_unhide!, blocks_at[index])
          else
            colsize!(layout_row, index, Fixed(0))
            colgap!(layout_row, index, 0)
            foreach(block_hide!, blocks_at[index])
          end
        end
        cb.blockscene.visible[] = true

      # hide everything
      else
        rowsize!(box_layout, 1, Fixed(0))
        foreach(block_hide!, blocks)
        foreach(block_hide!, reduce(vcat, blocks_at))
      end
    end

    append!(observers, obs_show)
  end

  @info "TargetRow: widget created"
  TargetRow()
end

