
"""
Widget that summarizes the UMOT problem.

### Interaction context
Derives from the observable
  * `:problem`
"""
struct ProblemOverview <: Widget end

function ProblemOverview( box_layout,
                          ctx :: Context;
                          title = "UMOT Problem" )

  context_require(ctx, :problem)
  @debug "ProblemOverview: creating widget"

  problem = ctx[:problem]
  layout = boxed_layout(box_layout, title)

  ntargets = @lift $problem |> length |> string
  resolution = @lift ($problem.cost.n, $problem.cost.m) |> string
  cost = @lift $problem.cost |> string 
  penalty = @lift $problem.penalty |> typeof |> nameof |> string
  mass_ref = @lift begin
    mass = MuSink.reference_product_mass($problem)
    @sprintf "%.3g" mass
  end

  Label(layout[1,1], "targets", halign = :left)
  Label(layout[end,2], ntargets, halign = :left)

  Label(layout[end+1,1], "resolution", halign = :left)
  Label(layout[end,2], resolution, halign = :left)

  Label(layout[end+1,1], "topology", halign = :left)
  Label(layout[end,2], "sequential", halign = :left)

  Label(layout[end+1,1], "cost", halign = :left)
  Label(layout[end,2], cost, halign = :left)

  Label(layout[end+1,1], "penalty", halign = :left)
  Label(layout[end,2], penalty, halign = :left)

  Label(layout[end+1,1], "ref. mass", halign = :left)
  Label(layout[end,2], mass_ref, halign = :left)

  # Label(layout[end+1,1], "pen. mass", halign = :left)
  # Label(layout[end,2], penalty, halign = :left)

  @info "ProblemOverview: widget created"
  ProblemOverview()
end
