
"""
Widget that visualizes the marginals of a workspace in a row

### Interaction context
  * :problem
  * :workspace
  * :area
  * :visible_nodes
  * :show_marginals
"""
struct MarginalRow <: Widget end

function MarginalRow(box_layout, ctx :: Context; title = "Marginal")
  @debug "MarginalRow: creating widget"
  context_require(ctx, :workspace, :area, :visible_nodes, :show_marginals)

  problem = ctx[:problem]
  selection = ctx[:visible_nodes]
  show = ctx[:show_marginals]

  blocks = []
  blocks_at = []
  observers = []

  lift(problem) do problem
    @debug "MarginalRow: drawing contents"

    foreach(block_delete!, blocks)
    foreach(block_delete!, reduce(vcat, blocks_at, init = []))
    foreach(off, observers)

    empty!(blocks)
    empty!(blocks_at)
    empty!(observers)

    layout = boxed_layout_horizontal(box_layout, title; blocks)
    layout_row = layout[1,1] = GridLayout(default_colgap = 10)

    n, m, _ = size(problem)
    mi, ma = Observable(Inf), Observable(-Inf)

    images = [Observable(zeros(n, m)) for _ in 1:length(problem)]

    for index in 1:length(problem)
 
      color = (Makie.COLOR_ACCENT_DIMMED[], 0.2)
      box = Box(layout_row[1, index]; strokewidth = 1, color, strokecolor = color)

      ax = Axis(layout_row[1, index]; aspect = DataAspect(), tellheight = false)

      img = images[index]
      image!(
        ax,
        @lift(rotr90($img)),
        colorrange = @lift(($mi, $ma))
      )

      hidedecorations!(ax)
      deregister_interactions!(ax)
      register_multipointer!(ax, ctx)

      obs = on(ctx[:area], update = true) do area
        start_i, stop_i, start_j, stop_j = area
        ax.limits[] = ((start_j-1, stop_j), (start_i-1, stop_i))
      end

      push!(blocks_at, [])
      push!(blocks_at[index], box, ax)
      push!(observers, obs)
    end

    obs_update = onany(ctx[:update], selection) do _, sel
      @debug "MarginalRow: received update signal"
      @async begin
        tmp_mi, tmp_ma = Inf, -Inf
        imgs = query_workspace(ctx, :get_marginal) do ws
          map(sel) do index
            img = MuSink.marginal(ws, index)
            convert(Array, dropdims(img, dims = 3))
          end
        end
        for i in 1:length(sel)
          images[sel[i]][] = imgs[i]
          if all(isfinite, imgs[i])
            tmp_mi = min(tmp_mi, minimum(imgs[i]))
            tmp_ma = max(tmp_ma, maximum(imgs[i]))
          else
            @debug "MarginalRow: non-finite values encountered in marginal"
          end
        end

        if isfinite(tmp_mi) && isfinite(tmp_ma)
          mi[] = tmp_mi
          ma[] = tmp_ma
        end
      end
    end

    # idx = findfirst(x -> isa(x, Axis), objects)
    # height = @lift $(objects[idx].scene.px_area).widths[2]

    cb =
      Colorbar(
        layout_row[1,length(problem)+1],
        colorrange = @lift(($mi, $ma)),
        colormap = [:white, :black],
        # height = height,
        width = 10,
        tellheight = false,
        ticks = @lift([$mi, ($ma + $mi)/2, $ma]),
        tickformat = "{:.1e}",
        ticklabelspace = 50
      )

    # Show only selected parts
    obs_show = onany(show, selection) do show, sel
      # show visible (selected) nodes
      if show
        rowsize!(box_layout, 1, Auto())
        foreach(block_unhide!, blocks)

        for index in 1:length(problem)
          if index in sel
            colsize!(layout_row, index, Auto())
            colgap!(layout_row, index, 10)
            foreach(block_unhide!, blocks_at[index])
          else
            colsize!(layout_row, index, Fixed(0))
            colgap!(layout_row, index, 0)
            foreach(block_hide!, blocks_at[index])
          end
        end
        cb.blockscene.visible[] = true

      # hide everything
      else
        rowsize!(box_layout, 1, Fixed(0))
        foreach(block_hide!, blocks)
        foreach(block_hide!, reduce(vcat, blocks_at))
      end
    end

    append!(observers, obs_show)

    push!(blocks, cb)
    append!(observers, obs_update)
    nothing
  end

  @info "MarginalRow: widget created"
  MarginalRow()
end

