
function parse_nodelist(str)
  str = replace(str, " " => "")
  sels = split(str, ",")
  nodes = Int[]
  for sel in sels
    if occursin(r"^[0-9]+$", sel) # have an Integer
      push!(nodes, parse(Int, sel))
    else # try to parse range
      m = match(r"([0-9]+)-([0-9]+)", sel)
      isnothing(m) && return
      a, b = parse.(Int, m.captures)
      append!(nodes, collect(a:b))
    end
  end
  sort(nodes)  
end

function validator_nodelist(len)
  str -> begin
    nodes = parse_nodelist(str)
    if isnothing(nodes)
      false
    else
      length(nodes) > 0 && all(x -> 1 <= x <= len, nodes)
    end
  end
end


"""
Widget that determines which parts of which nodes the viewer currently sees.
Contains a preview image where the currently viewed area is visualized.
Contains switches to activate / deactivate target, marginal, or diff views.
Contains a slider to select the nodes that are visualized.

### Interaction context
Derives from the observable
  * `:problem`

Registers the observables
  * `:area`
  * `:visible_nodes`
  * `:show_nodes`
  * `:show_targets`
  * `:show_marginals`
  * `:show_differencess`
  * `:multipointer`
  * `rows`
"""
struct AreaViewSelect <: Widget end

function AreaViewSelect( box_layout,
                         ctx :: Context;
                         title = "View",
                         preview_width = 150 )

  context_require(ctx, :problem)
  context_provide(
    ctx,
    :area,
    :visible_nodes,
    :show_targets,
    :show_marginals,
    :show_differences,
    :multipointer,
    :rows
  )

  @debug "AreaViewSelect: creating widget"

  problem = ctx[:problem]

  n = @lift size($problem)[1]
  m = @lift size($problem)[2]

  layout, reset = boxed_layout_reset(box_layout, title, false)

  layout_options = layout[1,1] = GridLayout(default_rowgap = 7, valign = :top)
  layout_preview = layout[1,2] = GridLayout(default_rowgap = 7, valign = :top)

  # View preview

  sel_i =
    IntervalSlider(
      layout_preview[1,1],
      range = @lift(1:1:$n),
      horizontal = false,
      tellwidth = true,
      tellheight = false,
    )

  sel_j =
    IntervalSlider(
      layout_preview[2,2],
      range = @lift(1:1:$m),
      horizontal = true,
      tellwidth = false,
      tellheight = true,
    )

  # Provide the entry :rows
  ctx[:rows] = Observable([])

  # Provide the entry :area to the interaction context
  ctx[:area] = @lift begin
    area = ($(sel_i.interval)..., $(sel_j.interval)...)
    @debug "AreaViewSelect: set option :area to $area"
    area
  end

  on(reset.clicks) do _
    @debug "AreaViewSelect: resetting area"
    set_close_to!(sel_i, 1, n[])
    set_close_to!(sel_j, 1, m[])
  end

  template = lift(problem) do problem
    root = problem.root
    reshape(problem.targets[root], n[], m[])
  end

  ma = @lift maximum($template)
  mi = @lift minimum($template)

  ax =
    Axis(
      layout_preview[1,2],
      aspect = DataAspect(),
      width = preview_width,
      height = @lift($n * preview_width / $m)
    )
  deregister_interactions!(ax)
  hidedecorations!(ax)

  # TODO: more convenient interactions!
  # Makie.register_interaction!(ax, :dragarea, AreaZoom(ctx))
    
  preview = @lift begin
    start_i, stop_i = $(sel_i.interval)
    start_j, stop_j = $(sel_j.interval)
    start_i, stop_i = $n-stop_i+1, $n-start_i+1 # take reflection into account

    # De-emphasize part of template that is not picked in the selected region
    img = copy($(template))
    img[start_i:stop_i, start_j:stop_j] .*= 5
    img ./= 5

    # Draw lines around the region
    img[[start_i, stop_i], start_j:stop_j] .= 0.8 * $ma
    img[start_i:stop_i, [start_j, stop_j]] .= 0.8 * $ma

    rotr90(img)
  end

  colorrange = @lift ($mi, $ma)
  image!(ax, preview; colorrange, colormap = [:black, :white], margin = 0.0)

  colgap!(layout_preview, 1, 7)

  # Select visible image rows
  objects = []
  observers = []
  on(ctx[:rows], update = true) do rows
    foreach(delete!, objects)
    foreach(off, observers)
    
    layout = layout_options[1, 1] = GridLayout(default_rowgap = 7)
    push!(objects,
      Label(layout[1,1], "row"),
      Label(layout[1,2], "view"),
      Label(layout[1,3], "visible"),
    )

    menu_options = (
      width = 75,
      textpadding = (8,8,4,4),
      selection_cell_color_inactive = :white,
      options = [
        Views.EmptyView,
        Views.TargetView,
        Views.MarginalView,
        Views.DifferenceView,
        Views.CostView,
      ]
    )

    for (i, row) in enumerate(rows)
      label = Label(layout[i+1, 1], string(i))
      default = typeof(row.view[])
      menu = Menu(layout[i+1, 2]; default = Makie.optionlabel(default), menu_options... )
      toggle = Toggle(layout[i+1, 3], active = row.visible[])

      obs_menu = onany(ctx[:workspace], menu.selection) do _, view
        @debug "AreaViewSelect: set option :show_targets to $active"
        row.view[] = view(ctx)
      end

      obs_toggle = on(toggle.active) do active
        row.visible[] = active
      end

      push!(objects, label, menu, toggle)
      push!(observers, obs_menu..., obs_toggle)
      i > 1 && rowgap!(layout, i, 0)
    end
  end

  # Active nodes
  ctx[:show_nodes] = Observable(true)
  layout_nodes = layout_options[end+1, 1] = GridLayout()

  Label(layout_nodes[1,1], "nodes", halign = :left, tellwidth = false)
  tb =
    Textbox(
      layout_nodes[1,2],
      stored_string = @lift(@sprintf "1-%d" length($problem)),
      validator = @lift(validator_nodelist(length($problem)))
    )

  ctx[:visible_nodes] = map(tb.stored_string) do str
    nodes = parse_nodelist(str)
    @debug "AreaViewSelect: set option :visible_nodes to $nodes"
    nodes
  end

  
  # Label(layout_options[end+1,1], "targets", halign = :left)
  # t = Toggle(layout_options[end,2], halign = :right, active = true)
  # ctx[:show_targets] = lift(t.active) do active
  #   @debug "AreaViewSelect: set option :show_targets to $active"
  #   active
  # end
  
  # Label(layout_options[end+1,1], "marginals", halign = :left)
  # t = Toggle(layout_options[end,2], halign = :right, active = true)
  # ctx[:show_marginals] = lift(t.active) do active
  #   @debug "AreaViewSelect: set option :show_marginals to $active"
  #   active
  # end

  # Label(layout_options[end+1,1], "differences", halign = :left)
  # t = Toggle(layout_options[end,2], halign = :right, active = true)
  # ctx[:show_differences] = lift(t.active) do active
  #   @debug "AreaViewSelect: set option :show_differences to $active"
  #   active
  # end

  # Multipointer
  layout_multi = layout_options[end+1, 1] = GridLayout()

  Label(layout_multi[1,1], "multipointer", halign = :left, tellwidth = false)
  toggle = Toggle(layout_multi[1,2], halign = :right, active = false)

  active = lift(toggle.active) do active
    @debug "AreaViewSelect: enable multipointer: $active"
    active
  end

  pointer = Interactions.MultiPointer(active)
  ctx[:multipointer] = Observable(pointer)

  rowgap!(layout_options, 1, 20)
  # rowgap!(layout_options, 2, 20)

  @info "AreaViewSelect: widget created"
  AreaViewSelect()
end

