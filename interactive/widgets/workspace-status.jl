
"""
Widget that displays the status of the current workspace.

### Interaction context
Requires the observables
  * `:workspace`
  * `:workspace_reset`
  * `:update`
  * `:update_interval` <- `:user`
  * `:blocking_queries`
"""
struct WorkspaceStatus <: Widget end

function WorkspaceStatus( box_layout,
                          ctx :: Context;
                          title = "Status",
                          iterations = 2000,
                          min = 1e-4 )

  context_require(ctx, :workspace, :workspace_reset, :update) #, :update_interval)
  @debug "WorkspaceStatus: creating widget"

  add_to_head = layout -> begin
    Label(layout[1,1], "update interval")
    Label(layout[1,2], "minor")
    tb_minor =
      Textbox(
        layout[1,3],
        stored_string = "20",
        validator = Int,
        tellheight = false
      )
    Label(layout[1,4], "major")
    tb_major =
      Textbox(
        layout[1,5],
        stored_string = "100",
        validator = Int,
        tellheight = false
      )
    colgap!(layout, 5)
    colgap!(layout, 1, 16)
    colgap!(layout, 3, 16)
    tb_minor, tb_major
  end

  ctx[:blocking_queries][] = vcat(ctx[:blocking_queries][], :update_minor)
  
  layout, (tb_minor, tb_major) = boxed_layout(box_layout, title, false; add_to_head)

  update_interval_minor = lift(tb_minor.stored_string) do str
    interval = parse(Int, str)
    @debug "WorkspaceStatus: set local option :update_interval_minor to $interval"
    interval
  end

  update_interval_major = lift(tb_major.stored_string) do str
    interval = parse(Int, str)
    @debug "WorkspaceStatus: set local option :update_interval_major to $interval"
    interval
  end

  on(update_interval_minor, update = true) do interval
    @debug "WorkspaceStatus: set option :update_interval to $interval"
    ctx[:update_interval][] = interval
  end

  # Keep track of major updates
  updates_major = Observable(0)
  onany(ctx[:workspace], update_interval_major) do _, _
    @debug "WorkspaceStatus: reset local value :updates_major to -1"
    updates_major[] = -1
  end


  names = [
    :speed => "steps / sec",
    :plan => "plan Δ",
    :marginals => "marginal Δ",
    :cost => "cost",
    :mass => "mass",
    :oscillation => "oscillation",
    :softness => "softness",
  ]

  objects = []
  observers = []

  on(ctx[:problem], update = true) do _
    @debug "WorkspaceStatus: draw content"

    foreach(delete!, objects)
    foreach(off, observers)
    empty!(objects)
    empty!(observers)

    iteration = Observable{Int}(0)
    vals = Dict(key => Observable(NaN) for  (key, _) in names)

    layout_graph = layout[1,1] = GridLayout()
    layout_value = layout[1,2] = GridLayout(default_rowgap = 4)

    colord = Dict()
    colors = Makie.current_default_theme().palette.color[] |> Iterators.cycle

    foreach(names, colors) do (key, _), color
      colord[key] = color
    end
  
    lb_name = Label(layout_value[1, 2], "iteration", halign = :left)
    lb_value = Label(layout_value[1, 3], @lift(string($iteration)), halign = :left)

    push!(objects, lb_name)
    push!(objects, lb_value)

    for (key, name) in names
      lb_bar = Label(layout_value[end+1, 1], "━━", font = :bold, color = colord[key])
      lb_name = Label(layout_value[end, 2], name, halign = :left)
      str = @lift begin
        val = $(vals[key])
        @sprintf "%.3g" val
      end
      lb_value = Label(layout_value[end, 3], str, halign = :left)
      push!(objects, lb_bar)
      push!(objects, lb_name)
      push!(objects, lb_value)
    end

    colgap!(layout_value, 1, 10)
    colgap!(layout_value, 2, 10)

    # The space for the values is reserved by the target color boxes
    colsize!(layout_value, 3, 0)
    

    time_last = Ref(0.0)
    time_diff = Ref(0.0)

    obs_update = on(ctx[:update]) do _

      time_now = time_ns()
      time_diff[] = 0.05 * time_diff[] + 0.95 * (time_now - time_last[]) / 1e9
      time_last[] = time_now

      @async begin
        steps = query_workspace(MuSink.steps, ctx, :steps)
        iteration[] = steps
        @debug "WorkspaceStatus: minor update"
        ret = query_workspace(ctx, :update_minor) do ws
          ( plan = MuSink.step_impact_plan(ws),
            marginals = MuSink.mean_marginal_deviation(ws) )
        end
        vals[:plan][] = ret.plan
        vals[:marginals][] = ret.marginals
        vals[:speed][] = ctx[:update_interval][] / time_diff[]
      end

      @async begin
        steps = query_workspace(MuSink.steps, ctx, :steps)
        updates = div(steps, update_interval_major[])
        if updates > updates_major[]
          if updates_major[] >= 0
            updates_major[] = updates
          else # reset signal
            updates_major[] = 0
          end
          @debug "WorkspaceStatus: major update"
          ret = query_workspace(ctx, :update_major) do ws
            ( cost = MuSink.cost(ws),
              mass = MuSink.mass(ws),
              oscillation = MuSink.mass_oscillation(ws),
              softness = MuSink.estimate_heavy_points(ws, samples = 10, threshold = 0.9) )
          end
          vals[:cost][] = ret.cost
          vals[:mass][] = ret.mass
          vals[:oscillation][] = ret.oscillation
          vals[:softness][] = ret.softness
        end
      end
    end

    push!(observers, obs_update)

    ax = Axis(
      layout_graph[1,1],
      yscale = log10,
      limits = (-iterations, 0, min, nothing)
    )
    deregister_interactions!(ax)
    push!(objects, ax)

    iters = Dict(key => Float64[0] for key in keys(vals))
    xdata = Dict(key => Observable(Float64[0]) for key in keys(vals))
    ydata = Dict(key => Observable(Float64[NaN]) for key in keys(vals))

    on(ctx[:workspace]) do _
      @debug "WorkspaceStatus: reset history due to workspace update"
      for key in keys(vals)
        empty!(iters[key])
        push!(iters[key], 0)
        xdata[key][] = Float64[0]
        ydata[key][] = Float64[NaN]
      end
    end

    lines = Dict{Symbol, Any}()
    for key in keys(vals)
      xs = xdata[key]
      ys = ydata[key]
      is = iters[key]

      lines[key] = lines!(ax, xs, ys, color = colord[key])

      obs = on(vals[key]) do value

        if value < 0
          @warn "WorkspaceStatus: received invalid value $value for key :$key"
          return
        end
        push!(ys[], value)
        push!(is, iteration[])
        push!(xs[], 0)
        xs[] .= iters[key] .- iteration[]

        if length(xs[]) > iterations
          popfirst!(is)
          popfirst!(xs[])
          popfirst!(ys[])
        end
    
        xs[] = xs[]
        ys[] = ys[]
      end
      push!(observers, obs)

    end
  end


  @info "WorkspaceStatus: widget created"
  WorkspaceStatus()
end

