

"""
Widget that visualizes several images in a row. The shown images correspond to
information derived from visible nodes.

### Interaction context
  * :problem
  * :workspace
  * :area
  * :visible_nodes
  * :rows

Additional context entries may be used by specific implementations.
"""
struct ImageRow <: Widget end

function defaults(pos, ctx)
  if pos == 1
    view = Views.TargetView(ctx)
    visible = true
  elseif pos == 2
    view = Views.MarginalView(ctx)
    visible = true
  elseif pos == 3
    view = Views.DifferenceView(ctx)
    visible = true
  else
    view = Views.EmptyView(ctx)
    visible = false
  end
  (view = Observable{Views.ImageView}(view), visible = Observable(visible))
end

function add_row!(ctx)
  pos = length(ctx[:rows][]) + 1
  push!(ctx[:rows][], defaults(pos, ctx))
  notify(ctx[:rows])
  @debug "ImageRow: registered $(view[]) at position $(pos)"
  pos
end

function update_minmax!(mi, ma, images, sel = 1:length(images))
  # Calculate and update extremal values
  tmp_mi, tmp_ma = Inf, -Inf
  for index in sel
    tmp_mi = min(tmp_mi, minimum(images[index][]))
    tmp_ma = max(tmp_ma, maximum(images[index][]))
  end

  # Prevent errors in Colorbar
  if tmp_mi == tmp_ma
    tmp_ma = tmp_mi + 1e-10
  end

  # Only update if the values are sound
  if isfinite(tmp_mi) && isfinite(tmp_ma)
    mi[] = tmp_mi
    ma[] = tmp_ma
    @debug "ImageRow: setting min/max to $(mi[])/$(ma[])"
  else
    @debug "ImageRow: non-finite values encountered in image view"
  end
end

function ImageRow(box_layout, ctx :: Context)
  @debug "ImageRow: creating widget"
  context_require(ctx, :workspace, :area, :visible_nodes, :rows)

  problem = ctx[:problem]
  selection = ctx[:visible_nodes]

  pos = add_row!(ctx)
  view = ctx[:rows][][pos].view
  visible = ctx[:rows][][pos].visible

  blocks = []
  blocks_at = []
  observers = []

  lift(problem) do problem
    @debug "ImageRow: drawing contents"

    # Cleanup if a previous problem introduced blocks or observers
    foreach(block_delete!, blocks)
    foreach(block_delete!, reduce(vcat, blocks_at, init = []))
    foreach(off, observers)

    empty!(blocks)
    empty!(blocks_at)
    empty!(observers)

    # Initialize color range
    mi, ma = Observable(Inf), Observable(-Inf)

    # Initialize observables that depend on the image view
    n, m, _ = size(problem)
    images = [Observable(zeros(n, m)) for _ in 1:length(problem)]
    title = Observable{String}("")

    # React to changes of the image view
    observer_update = []
    obs_view = on(view, update = true) do view
      @debug "ImageRow: view changes to $view)"
      context_require(ctx, Views.required_entries(view)...)

      foreach(off, observer_update)
      empty!(observer_update)

      # Adapt observables
      title[] = Views.title(view)
      Views.on_init!(images, view, ctx)
      Views.on_update!(images, view, ctx, 1:length(images))
      update_minmax!(mi, ma, images)

      # Make sure that visible content is up to date
      update = Views.updater(view, ctx)
      obs_update = onany(update, selection, visible) do _, sel, visible
        @debug "ImageRow: received update signal"
        if visible
          @async try
            Views.on_update!(images, view, ctx, sel)
            update_minmax!(mi, ma, images, sel)
          catch err
            @error "ImageRow: $err"
          end
        end
      end
      append!(observer_update, obs_update)
      
    end
    push!(observers, obs_view)

    # Initialize layout
    layout = boxed_layout_horizontal(box_layout, title; blocks)
    layout_row = layout[1,1] = GridLayout(default_colgap = 10)
    
    # React to changes of visibility and selection
    obs_visible = onany(visible, selection) do visible, sel
      # show visible (selected) nodes
      if visible
        rowsize!(box_layout, 1, Auto())
        foreach(block_unhide!, blocks)

        for index in 1:length(problem)
          if index in sel
            colsize!(layout_row, index, Auto())
            colgap!(layout_row, index, 10)
            foreach(block_unhide!, blocks_at[index])
          else
            colsize!(layout_row, index, Fixed(0))
            colgap!(layout_row, index, 0)
            foreach(block_hide!, blocks_at[index])
          end
        end
        cb.blockscene.visible[] = true

      # hide everything due to visible = false
      else
        rowsize!(box_layout, 1, Fixed(0))
        foreach(block_hide!, blocks)
        foreach(block_hide!, reduce(vcat, blocks_at))
      end
    end
    append!(observers, obs_visible)

    # Populate the slots of the row with axes
    for index in 1:length(problem)
 
      # Background box and axis
      # TODO: Allow MuSink Theming!
      color = (Makie.COLOR_ACCENT_DIMMED[], 0.2)
      box = Box(layout_row[1, index]; strokewidth = 1, color, strokecolor = color)
      ax = Axis(layout_row[1, index]; aspect = DataAspect(), tellheight = false)

      # Plot the image at index
      img = images[index]
      image!(
        ax,
        @lift(rotr90($img)),
        colorrange = @lift(($mi, $ma)),
        colormap = [:black, :white],
      )

      # Interactions for the axis
      hidedecorations!(ax)
      deregister_interactions!(ax)
      Interactions.register_multipointer!(ax, ctx)

      obs = on(ctx[:area], update = true) do area
        start_i, stop_i, start_j, stop_j = area
        ax.limits[] = ((start_j-1, stop_j), (start_i-1, stop_i))
      end

      # Register objects to be hidden / deleted if the visibility / problem changes
      push!(blocks_at, [])
      push!(blocks_at[index], box, ax)
      push!(observers, obs)
    end

    cb =
      Colorbar(
        layout_row[1,length(problem)+1],
        colorrange = @lift(($mi, $ma)),
        colormap = [:black, :white],
        width = 10,
        tellheight = false,
        ticks = @lift([$mi, ($ma + $mi)/2, $ma]),
        tickformat = "{:.1e}",
        ticklabelspace = 50
      )

    push!(blocks, cb)
    nothing
  end

  @info "ImageRow: widget created"
  ImageRow()
end


