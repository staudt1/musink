
"""
Widget to set the parameters eps, rho, and reach.  

### Interaction context
Requires the observable
  * `:problem`
  * `:workspace`
  * `:eps`
  * `:rho`
"""
struct WorkspaceParameters <: Widget end

function getrange_right(val)
  (10).^range(-2, 0, length = 1001) .* val
end

function getrange_center(val)
  (10).^range(-1, 1, length = 1001) .* val
end

function WorkspaceParameters( box_layout,
                              ctx :: Context;
                              title = "Parameters",
                              slider_width = 100 )

  context_require(ctx, :problem, :workspace)
  @debug "WorkspaceParameters: creating widget"

  layout, reset = boxed_layout_reset(box_layout, title, false)

  reach_max = map(ctx[:problem]) do problem
    n, m, _ = size(problem)
    max(n, m) - 1
  end

  ranges = [
    :eps => Observable(getrange_right(0.1)),
    :rho => Observable(getrange_right(1)),
    :reach => @lift(1:1:$reach_max)
  ]

  layout_maxes = layout[2, 1] =
    GridLayout(
      halign = :left,
      tellwidth = false,
      default_rowgap = 7
    )
  rowgap!(layout, 1, 20)
  
  # Allow user to set maximal values, which affect the ranges
  for i in 1:length(ranges)
    key = ranges[i][1]

    kwargs = (
      validator = key == :reach ? Int : Float64,
      stored_string = string(maximum(ranges[i][2][])),
      tellheight = false,
      # borderwidth = 0,
      halign = :left
    )

    Label(layout_maxes[i,1], "max $key", halign = :left)
    tb = Textbox(layout_maxes[i,2]; kwargs...)

    on(tb.stored_string) do s
      max = parse(Float64, s)
      if key == :reach
        ranges[i][2][] = 1:1:round(Int, max)
      else
        min = max / 100
        ranges[i][2][] = min:(min/100):max
      end
    end

  end

  # Sliders for fine-tuning of the parameters
  options = map(ranges) do (key, range)
    (; label = string(key), range, startvalue = @lift(maximum($range)))
  end
  sg = SliderGrid(layout[1,1], options...)
  rowgap!(sg.layout, 7)
  colgap!(sg.layout, 10)

  for slider in sg.sliders
    slider.width[] = slider_width
  end

  # Scrolling interaction to fine-tune the sliders
  for slider in sg.sliders
    range = slider.range
    scene = slider.blockscene
    bbox = slider.layoutobservables.computedbbox
    events = scene.events

    scroll = Observables.throttle(0.1, events.scroll)
    on(scroll) do event
      if events.mouseposition[] in bbox[]
        up = round(Int, event[2])
        if Keyboard.left_alt in events.keyboardstate
          idx = slider.selected_index[] + up
        else
          idx = slider.selected_index[] + 10up
        end
        idx = clamp(idx, 1, length(range[]))
        set_close_to!(slider, range[][idx])
      end
    end
  end

  # Right-click to recenter the current value for eps and rho
  for slider in sg.sliders[1:2]
    scene = slider.blockscene
    bbox = slider.layoutobservables.computedbbox
    event = addmouseevents!(scene, bbox)
    onmouserightclick(event) do _
      slider.range[] = getrange_center(slider.value[]) 
    end
  end

  on(reset.clicks) do _
    # TODO: this does not work properly!
    # TODO: Report this as bug / feature request?
    # tb_rho.displayed_string[] = "1.0"
    # tb_eps.displayed_string[] = "1.0"
    # tb_rho.stored_string[] = "1.0"
    # tb_eps.stored_string[] = "1.0"

    for slider in sg.sliders
      set_close_to!(slider, slider.startvalue[])
      set_close_to!(slider, slider.startvalue[])
    end
  end

  on(ctx[:workspace], update = true) do _
    @async query_workspace(ctx, :set_parameters) do ws
      MuSink.adapt_eps!(ws, sg.sliders[1].value[])
      MuSink.adapt_rho!(ws, sg.sliders[2].value[])
      MuSink.adapt_reach!(ws, sg.sliders[3].value[])
    end
  end

  obs_eps = sg.sliders[1].value
  on(sg.sliders[1].value, update = true) do eps
    @async query_workspace(ws -> MuSink.adapt_eps!(ws, eps), ctx, :adapt_eps)
  end

  obs_rho = sg.sliders[2].value
  on(obs_rho, update = true) do rho
    @async query_workspace(ws -> MuSink.adapt_rho!(ws, rho), ctx, :adapt_rho)
  end

  obs_reach = sg.sliders[3].value
  on(obs_reach, update = true) do reach
    @async query_workspace(ws -> MuSink.adapt_reach!(ws, reach), ctx, :adapt_reach)
  end

  @info "WorkspaceParameters: widget created"
  WorkspaceParameters()
end

