
"""
Widget to interact with workspace options.

### Interaction context
Requires the observables
  * `:workspace`
  * `:running`

Provides the observable
  * `:workspace_reset` <- `:user`
"""
struct WorkspaceOptions <: Widget end

function WorkspaceOptions( box_layout,
                           ctx :: Context,
                           title = "Workspace" )

  context_require(ctx, :workspace, :running)
  context_provide(ctx, :workspace_reset)
  @debug "WorkspaceOptions: creating widget"

  layout, reset = boxed_layout_reset(box_layout, title)
  ctx[:workspace_reset] = Observable(nothing)
  
  options = Dict{Symbol, Any}()

  toggles = Dict{Symbol, Any}()
  toggle_kwargs = (
    active = false,
    halign = :right,
    tellwidth = false
  )

  Label(layout[1,1], "logdomain", halign = :left)
  toggles[:logdomain] = Toggle(layout[end,2]; toggle_kwargs...)
  options[:logdomain] = toggles[:logdomain].active

  Label(layout[end+1,1], "float64", halign = :left)
  toggles[:float64] = Toggle(layout[end,2]; toggle_kwargs...)
  options[:float64] = toggles[:float64].active

  if CUDA.functional()
    Label(layout[end+1,1], "gpu", halign = :left)
    toggles[:gpu] = Toggle(layout[end,2]; toggle_kwargs...)
    options[:gpu] = toggles[:gpu].active
  else
    options[:gpu] = Observable(false)
  end

  on(options[:logdomain]) do logdomain
    @debug "WorkspaceOptions: set internal option :logdomain to $logdomain"
    query_workspace(ws -> MuSink.adapt_domain!(ws, logdomain), ctx, :logdomain)
  end

  onany(options[:gpu], options[:float64]) do gpu, float64
    F = float64 ? Float64 : Float32
    T = gpu ? CUDA.CuArray{F} : Array{F}
    @debug "WorkspaceOptions: set workspace array type $T"
    workspace = query_workspace(ws -> copy(ws, T), ctx, :copy_atype)
    ctx[:workspace][] = workspace
    if !(ctx[:workspace][] isa Workspace{T, F})
      @warn "WorkspaceOptions: update of workspace failed"
    end
  end

  on(reset.clicks) do _
    F = options[:float64][] ? Float64 : Float32
    T = options[:gpu][] ? CUDA.CuArray{F} : Array{F}
    @debug "WorkspaceOptions: resetting workspace"
    workspace = query_workspace(ctx, :reset) do ws
      Workspace(T, ws.problem; ws.eps, ws.rho, ws.rhos, ws.reach, ws.logdomain)
    end
    ctx[:workspace][] = workspace
    if !(ctx[:workspace][] isa Workspace{T, F})
      @warn "WorkspaceOptions: update of workspace failed"
    end
    notify(ctx[:update])
    notify(ctx[:workspace_reset])
  end


  color_running = "#AFC3B6"

  button_run =
    Button(
      layout[end+1, 1:end],
      label = "run",
      halign = :center,
      width = 75,
      buttoncolor = :gray95,
      buttoncolor_active = color_running,
      buttoncolor_hover = color_running,
      labelcolor_active = :black
    )

  on(button_run.clicks) do _
    running = !ctx[:running][]
    @debug "WorkspaceOptions: set option :running to $running"
    ctx[:running][] = running
  end

  color_animation = () -> @async begin
    time_start = time()
    while ctx[:running][]
      arg = 3(time() - time_start)
      intensity = (-cos(arg) + 3) / 4
      button_run.buttoncolor[] = (color_running, intensity)
      sleep(0.03)
    end
  end

  on(ctx[:running]) do running
    if running
      button_run.label[] = "running"
      color_animation()
    else
      button_run.buttoncolor[] = :gray95
      button_run.label[] = "run"
      button_run.labelcolor[] = :black
    end
  end

  scene = button_run.blockscene
  on(events(scene).keyboardbutton) do event
    if event.action == Keyboard.press && event.key == Keyboard.space
      running = !ctx[:running][]
      @debug "WorkspaceOptions: set option :running to $running (space key)"
      ctx[:running][] = running
    end
  end

  # Label(layout[1,1], "run steps", halign = :left)
  # toggles[:running] = Toggle(layout[end,2]; toggle_kwargs...)
  # options[:running] = toggles[:running].active

  # on(options[:running]) do running
  #   @debug "WorkspaceOptions: set option :running to $running"
  #   ctx[:running][] = running
  # end

  
  # Resetting the options does not work, because visuals of Sliders are not
  # updated when their value is set...
  # on(reset.clicks) do _
  #   for key in keys(options)
  #     options[key][] = false
  #   end
  # end

  rowgap!(layout, 7)

  rowgap!(layout, size(layout)[1]-1, 20)


  @info "WorkspaceOptions: widget created"
  WorkspaceOptions()
end
