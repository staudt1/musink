
"""
Widget to visualize transport options.

### Interaction context
Requires the observable
  * `:problem`
Provides the observable
  * `:update_transport`
"""
struct TransportOptions <: Widget end

function TransportOptions(box_layout, ctx; title = "Transport")

  context_require(ctx, :problem)
  @debug "TransportOptions: creating widget"

  problem = ctx[:problem]
  layout, reset = boxed_layout_reset(box_layout, title)
  
  toggle_kwargs = (
    active = false,
    halign = :right,
    tellwidth = false
  )
  
  options = Dict{Symbol, Any}()
  
  # Use Barycentric map or full distribution when visualizing transport
  Label(layout[1,1], "barycenter", halign = :left)
  toggle = Toggle(layout[end,2]; toggle_kwargs...) 
  options[:barycenter] = map(toggle.active) do active
    @debug "TransportOptions: set local option :barycenter to $active"
    active
  end
  
  Label(layout[end+1,1], "range", halign = :left)
  slider = Slider(layout[end,2], width = 50, range = @lift(1:1:length($problem)))
  Label(layout[end+1,2], @lift(string($(slider.value))))
  options[:range] = map(slider.value) do value
    @debug "TransportOptions: set local option :range to $value"
    value
  end

  button = Button(layout[end+1, 1:2], label = "update", halign = :center)
  ctx[:update_transport] = map(button.clicks) do click
    @debug "TransportOptions: clicked update button"
    click
  end
  
  rowgap!(layout, 2, 0)
  
  # TODO: reset!
  
  @info "TransportOptions: widget created"
  TransportOptions()
end

