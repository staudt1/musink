
"""
Widget that presents basic information about the nodes of a MuSink problem.

### Interaction context
  * :problem
  * :visible_nodes
  * :node_names
  * :workspace
"""
struct NodeRow <: Widget end

function NodeRow(box_layout, ctx :: Context; title = "Node")
  @debug "NodeRow: creating widget"
  context_require(ctx, :problem, :node_names, :workspace)

  problem = ctx[:problem]
  names = ctx[:node_names]
  selection = ctx[:visible_nodes]
  show = ctx[:show_nodes]

  blocks = []
  blocks_at = []
  observers = []

  lift(problem, names) do problem, names
    @debug "NodeRow: drawing contents"

    foreach(block_delete!, blocks)
    foreach(block_delete!, reduce(vcat, blocks_at, init = []))
    foreach(off, observers)

    # empty!(box_layout.content)
    empty!(blocks)
    empty!(blocks_at)
    empty!(observers)

    layout = boxed_layout_horizontal(box_layout, title; blocks)
    layout_row = layout[1,1] = GridLayout(default_colgap = 10)

    for index in 1:length(problem)

      node = Tree.descendant(problem.root, index)
      name = names[node]
      mass = sum(problem.targets[node])

      layout_aux = layout_row[1, index] = GridLayout(
        default_colgap = 10,
        tellwidth = false,
        halign = :center
      )

      layout_info = layout_aux[1, 1] = GridLayout(
        halign = :left,
        alignmode = Outside(20, 0, 0, 0),
        default_colgap = 10,
        default_rowgap = 7
      )

      layout_options = layout_aux[1, 2] = GridLayout(
        halign = :right,
        alignmode = Outside(0, 20, 0, 0),
        default_rowgap = 7,
        default_colgap = 10
      )

      halign = :left
      lb1 = Label(layout_info[1,1], "$index"; halign, font = :bold, fontsize = 14)
      lb2 = Label(layout_info[2,1], name; halign, font = :italic)

      # TODO: make name hide if space is too small
      # on(layout_info.layoutobservables.suggestedbbox) do bbox
      #   @show bbox
      #   lb2_bbox = lb2.layoutobservables.computedbbox[]
      #   if any(bbox.widths .< lb2_bbox.widths)
      #     block_hide!(lb2)
      #   else
      #     block_unhide!(lb2)
      #   end
      # end

      lb3 = Label(layout_options[2,2], "mass"; halign)
      lb4 = Label(layout_options[3,2], "rho"; halign)

      rho = query_workspace(ctx, :get_rho_at) do ws
        if haskey(ws.rhos, node)
          ws.rhos[node]
        else
          -1
        end
      end

      mass_str = @sprintf "%.2g" mass
      rho_str = @sprintf "%.2g" rho

      tb_mass =
        Textbox(
          layout_options[2,3];
          stored_string = mass_str,
          validator = Float64,
          width = 40,
          halign,
          tellheight = false
        )

      tb_rho =
        Textbox(
          layout_options[3,3];
          stored_string = rho_str,
          validator = Float64,
          width = 40,
          halign,
          tellheight = false
        )

      obs = on(tb_rho.stored_string, update = true) do str
        @debug "NodeRow: set value rho for node $index to $str"
        rho = parse(Float64, str)
        rho = rho >= 0 ? rho : nothing
        @async query_workspace(ctx, :adapt_rho_at) do ws
          MuSink.adapt_rho!(ws, node, rho)
        end
      end

      # TODO: Allow update of target mass!

      push!(blocks_at, [])
      push!(blocks_at[index], lb1, lb2, lb3, lb4, tb_mass, tb_rho)

      push!(observers, obs)
      nothing
    end

    # Artificially create some space that is used for colorbar in other rows
    layout_row[1,length(problem)+1] = GridLayout(width = 10)

    # Show only selected parts
    obs_show = onany(show, selection) do show, sel
      # show visible (selected) nodes
      if show
        rowsize!(box_layout, 1, Auto())
        foreach(block_unhide!, blocks)

        for index in 1:length(problem)
          if index in sel
            colsize!(layout_row, index, Auto())
            colgap!(layout_row, index, 10)
            foreach(block_unhide!, blocks_at[index])
          else
            colsize!(layout_row, index, Fixed(0))
            colgap!(layout_row, index, 0)
            foreach(block_hide!, blocks_at[index])
          end
        end

      # hide everything
      else
        rowsize!(box_layout, 1, Fixed(0))
        foreach(block_hide!, blocks)
        foreach(block_hide!, reduce(vcat, blocks_at))
      end
    end

    append!(observers, obs_show)
    @debug "NodeRow: contents drawn"
  end
  
  @info "NodeRow: widget created"
end