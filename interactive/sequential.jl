
using Revise
using TiffImages
using GLMakie

include("interactive.jl")

# ---- Image paths for testing ----------------------------------------------- #

tiff_paths = [
    "data/14_cell_C001T001.tiff"
  , "data/14_cell_C001T002.tiff"
  , "data/14_cell_C001T003.tiff"
  , "data/14_cell_C001T004.tiff"
  , "data/14_cell_C001T005.tiff"
  #   "data/simple-1.tif"
  # , "data/simple-2.tif"
  # , "data/simple-2.tif"
  # , "data/simple-2.tif"
  #   "data/test-1.tif"
  # , "data/test-2.tif"
  # , "data/test-3.tif"
  # , "data/test-4.tif"
  # , "data/test-5.tif"
]

# ----- Load images ---------------------------------------------------------- #

"""
    load_tiff_images(paths; offset = 1e-5)

Load several tiff images residing at `paths`. The images are affinely
normalized such that their minimal value is `offset` and their value sum is 1.
"""
function load_tiff_images(paths; offset = 1e-5, invert = false)
  images = map(paths) do path
    tiff = TiffImages.load(path)
    image = convert(Array{Float64}, tiff)
    if invert
      image .= .- image
    end
    min = minimum(image)
    image = (image .- min .+ offset)
    image ./ sum(image)
  end
  n, m = size(first(images))
  @assert all(img -> size(img) == (n, m), images) "Images have different resolution"
  images
end

theme = Theme(
  # font = "Ubuntu",
  # labelfont = "Ubuntu",
  # titlefont = "Fira Sans",
  fontsize = 12,
  Image = (
    colormap = [:white, :black],
    interpolate = false
  ),
  Legend = (
    # labelfont = "Ubuntu",
    labelsize = 12,
  ),
  Axis = (
    yticklabelsize = 12,
    xticklabelsize = 12,
  ),
  Slider = (
    linewidth = 10,
  ),
  IntervalSlider = (
    linewidth = 10,
  ),
  Toggle = (
    height = 14,
    width = 32,
  ),
  Textbox = (
    cornerradius = 4,
    reset_on_defocus = true,
    textpadding = (8, 8, 1, 1)
  )
  
  # Label = (
  #   font = "Ubuntu",
  # )
)

images = load_tiff_images(tiff_paths)

problem = begin
  targets = images
  n, m = size(first(targets))

  cost = MuSink.Lp(n, m, p = 1)
  penalty = MuSink.TotalVariation()

  reference_mass = median(sum.(images))
  root = Tree.Sequence(length(images))
  # root = Tree.descendant(root, 2) |> Tree.reroot
  targets = Dict(
    Tree.descendant(root, i) => images[i][:,:,:] for i in 1:length(images)
  )
  references = MuSink.default_references(targets, reference_mass)

  problem = MuSink.Problem(targets; cost, penalty, references)
  Observable(problem)
end

names = map(enumerate(tiff_paths)) do (index, path)
  Tree.descendant(problem[].root, index) => basename(path)
end |> Dict |> Observable


ctx = Context(problem; node_names = names)
init(ctx, threaded = true)


# init_loop(ctx)

fig = with_theme(theme) do
  fig = Figure(font = "Ubuntu", resolution = (1600, 800))
  register!(ctx, fig)

  top_row = fig[1,1] = GridLayout(tellwidth = false, valign = :top)

  layout = top_row[1,1] = GridLayout(valign = :top)
  Widgets.ProblemOverview(layout, ctx)

  layout = top_row[1,2] = GridLayout(valign = :top)
  Widgets.WorkspaceOptions(layout, ctx)

  layout = top_row[1,3] = GridLayout(valign = :top)
  Widgets.WorkspaceParameters(layout, ctx)

  layout = top_row[1,4] = GridLayout(valign = :top)
  Widgets.TransportOptions(layout, ctx)

  layout = top_row[1,5] = GridLayout(valign = :top)
  Widgets.AreaViewSelect(layout, ctx)

  layout = top_row[1,6] = GridLayout(valign = :top)
  Widgets.WorkspaceStatus(layout, ctx)

  body = fig[2,1] = GridLayout(tellwidth = false, default_rowgap = 15)

  mid_row = body[1,1] = GridLayout(tellwidth = false)
  Widgets.NodeRow(mid_row, ctx)

  mid_row = body[2,1] = GridLayout(tellwidth = false)
  # Widgets.TargetRow(mid_row, ctx)
  Widgets.ImageRow(mid_row, ctx)

  mid_row = body[3,1] = GridLayout(tellwidth = false)
  # Widgets.MarginalRow(mid_row, ctx)
  Widgets.ImageRow(mid_row, ctx)

  mid_row = body[4,1] = GridLayout(tellwidth = false)
  Widgets.ImageRow(mid_row, ctx)
  # Widgets.DifferenceRow(mid_row, ctx)

#   # targets = widget_images!(layout, as.area, images; title = "Targets", names)
#   # for i in 1:length(images)
#   #   targets[i].bg[] = rotr90(images[i])
#   # end

#   # mid_row = fig[3,1] = GridLayout()
#   # layout = mid_row[1,1] = GridLayout()
#   # marginals = widget_images!(layout, ctx.area, images, title = "Marginals")

#   # layout = top_row[1,2] = GridLayout()
#   # targets = widget_images!(layout, area, images)
  
  notify(ctx[:update])
  rowgap!(fig.layout, 0)
  fig
end

# function init_sinkhorn_steps(targets, marginals, options, status, params)

#   problem = begin
#     targets = [target[] for image in targets]
#     n, m = size(first(targets))

#     cost = MuSink.Lp(n, m, p = 1.0)
#     penalty = options[:penalty][]

#     reference_mass = median(sum.(images))
#     root = Tree.Sequence(length(images))
#     targets = Dict(
#       Tree.descendant(root, i) => images[i][:,:,:] for i in 1:length(images)
#     )
#     references = MuSink.default_references(targets, reference_mass)
  
#     MuSink.Problem(targets; cost, penalty, references)
#   end

# end


# ----- Create figure -------------------------------------------------------- #

#theme = Theme(font = "Ubuntu Mono", titlefont = "Ubuntu Mono")
#set_theme!(theme)
# fig = Figure()

# marginals_o = map(1:length(images)) do i
#   Observable(images[i])
# end

# for i in 1:length(images)
#   path = tiff_paths[i]

#   axis_options = (
#     xticksvisible = false,
#     xticklabelsvisible = false,
#     yticksvisible = false,
#     yticklabelsvisible = false
#   )

#   image_options = (
#     interpolate = false,
#     colormap = [:black, :white]  
#   )

#   sel_i = 100:175
#   sel_j = 100:175

#   ax_target = Axis(fig[3,i]; title = path, aspect = DataAspect(), axis_options...)
#   image = images[i][sel_i,sel_j]
#   image!(ax_target, image; image_options...)

#   ax_marginal = Axis(fig[5,i]; aspect = DataAspect(), axis_options...)
#   marginal = lift(m -> m[sel_i, sel_j], marginals_o[i])
#   image!(ax_marginal, marginal; image_options...)

#   ax_marginal = Axis(fig[4,i]; aspect = DataAspect(), axis_options...)
#   difference = lift(m -> .- (m[sel_i, sel_j] .- images[i][sel_i, sel_j]), marginals_o[i])
#   image!(ax_marginal, difference; image_options...)
# end

# sg = SliderGrid(fig[1,1:length(images)], 
#   (label = "eps", range = 0.0005:0.0005:0.01, startvalue = 0.01),
#   (label = "rho", range = 0.001:0.0001:0.1, startvalue = 0.1),
#   (label = "reach", range = 1:1:50, startvalue = 50)
# )

# #rsi = IntervalSlider(fig[4, 2:length(images)])

# info = Observable("impact  --\ndiff    --\nmass    --")
# Label(fig[2, 1:length(images)], text = info, justification = :left, halign = :left, font = "Ubuntu Mono")

# # ----- Define MuSink problem ------------------------------------------------ #

# problem = begin
#   cost = MuSink.Lp(n, m, p = 1.0)
#   penalty = MuSink.TotalVariation()
#   reference_mass = median(sum.(images))
#   root = Tree.Sequence(length(images))
#   targets = Dict(
#     Tree.descendant(root, i) => images[i][:,:,:] for i in 1:length(images)
#   )
#   references = MuSink.default_references(targets, reference_mass)

#   MuSink.Problem(targets; cost, penalty, references)
# end

# workspace = begin
#   logdomain = true
#   MuSink.Workspace(Array{Float64}, problem; eps = 0.01, rho = 0.1, logdomain)
# end

# # ----- Run the simulation :) ------------------------------------------------ #

# function run_steps()
#   eps_s = sg.sliders[1]
#   rho_s = sg.sliders[2]
#   reach_s = sg.sliders[3]
#   steps = 5
#   eps = 0.01
#   rho = 0.1
#   reach = 100
#   while true
#     if eps != eps_s.value[]
#       eps = eps_s.value[]
#       @show eps
#       MuSink.adapt_eps!(workspace, eps)
#     end
#     if rho != rho_s.value[]
#       @show rho
#       rho = rho_s.value[]
#       MuSink.adapt_rho!(workspace, rho)
#     end
#     if reach != reach_s.value[]
#       @show reach
#       reach = reach_s.value[]
#       MuSink.adapt_reach!(workspace, reach)
#     end
#     MuSink.steps!(workspace, steps)
#     for i in 1:length(marginals_o)
#       marginals_o[i][] = MuSink.marginal(workspace, i)[:,:,1]
#     end

#     impact = MuSink.step_impact_plan(workspace)
#     diff = mean(abs, MuSink.marginal(workspace, 1)[:,:,1] .- images[1])
#     mass = MuSink.mass(workspace)
#     ref = sum(images[1])
    
#     str = @sprintf "impact %10.3g\ndiff   %10.3g\nmass   %10.3g (%.2f)" impact diff mass (mass/ref)
#     @show str
#     info[] = str
#   end
# end
