
module Interactions

  using GLMakie
  import ..Context

  struct MultiPointer
    active :: Observable{Bool}
    color_active :: Observable{Any}
    color :: Observable{Any}
    radius :: Observable{Int}
    linewidth :: Observable{Int}
    position :: Observable{Point2f}
  end

  function MultiPointer(active :: Observable{Bool} = Observable(false))
    color_active = Observable(:red)
    color = @lift $active ? $color_active : :transparent
    radius = Observable(9)
    linewidth = Observable(2.)
    position = Observable(Point2f(0,0))

    MultiPointer(
      active,
      color_active,
      color,
      radius,
      linewidth,
      position
    )
  end

  function Makie.process_interaction(pointer :: MultiPointer, event :: MouseEvent, ax :: Axis)
    @assert pointer.active[] "Cannot process inactive MultiPointer interaction"

    transf = Makie.transform_func(ax)
    inv_transf = Makie.inverse_transform(transf)

    if isnothing(inv_transf)
      @warn "Can't show MultiPointer without inverse transform" maxlog = 1
      return Consume(false)
    end

    if event.type == MouseEventTypes.over
      data = Makie.apply_transform(inv_transf, event.data)
      pointer.position[] = data
    end
  end

  function register_multipointer!(ax :: Axis, ctx :: Context)
    objects = []

    on(ctx[:multipointer], update = true) do pointer

      axid = hash(ax)

      if :multipointer in keys(Makie.interactions(ax))
        Makie.deregister_interaction!(ax, :multipointer)
        for obj in objects
          delete!(ax.scene, obj)
        end
      end

      Makie.register_interaction!(ax, :multipointer, pointer)

      on(pointer.active, update = true) do active
        if !active
          Makie.deactivate_interaction!(ax, :multipointer)
        else
          Makie.activate_interaction!(ax, :multipointer)
        end
      end

      marker = Makie.scatter!(
        ax,
        pointer.position;
        markersize = pointer.radius,
        pointer.color,
        strokewidth = 0,
        glowwidth = 4,
      )
      push!(objects, marker)
    end
  
  end

  # TODO: this does not work yet...
  struct AreaZoom
    resolution :: Tuple{Int, Int}
    area :: Observable{NTuple{4, Int}}
  end

  function AreaZoom(ctx :: Context)
    resolution = size(ctx[:problem][])[1:2]
    AreaZoom(resolution, ctx[:area])
  end

  function Makie.process_interaction(az :: AreaZoom, event :: ScrollEvent, ax :: Axis)

    n, m = az.resolution
    i_range = az.area[][1:2]
    j_range = az.area[][3:4]
    center_px = events(ax.blockscene).mouseposition[]

    up = event.y > 0
    factor = up ? 0.95 : 1.05

    transf = Makie.transform_func(ax)
    inv_transf = Makie.inverse_transform(transf)

    center = Makie.apply_transform(transf, center_px)
    @show center_px
    @show center
    center = clamp.(center, (1, 1), (n, m))
    @show center

    i_center = center[1]
    j_center = center[2]

    i_range = factor .* (i_range .- i_center) .+ i_center
    j_range = factor .* (j_range .- j_center) .+ j_center

    i_range = round.(Int, clamp.(i_range, 1, n))
    j_range = round.(Int, clamp.(j_range, 1, m))

    az.area[] = (i_range..., j_range...)
  end

  struct AreaDrag
   n :: Int
   m :: Int
   start_area :: Ref{Tuple{4, Float64}}
   start_anchor :: Ref{Point2f}
  end

  function Makie.process_interaction(da :: AreaDrag, event :: MouseEvent, ax :: Axis)

    if event.type !== MouseEventTypes.rightdragstart
      da.anchor[] = event.px
      return Consume()
    end

    if event.type !== MouseEventTypes.rightdrag
      return Consume(false)
    end
    diff = event.px - da.anchor[]
  
  end

end # Interactions