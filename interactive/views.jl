
module Views

  using Statistics

  import MuSink
  import MuSink: Tree
  using GLMakie

  import ..Context
  import ..query_workspace

  """
  Map a context to images.

  Concrete subtypes implement various image representations of the MuSink
  problem or workspace.
  """
  abstract type ImageView end

  name(:: Type{ImageView}) = :generic
  name(view :: ImageView) = name(typeof(view))

  function title(view :: ImageView)
    str = string(name(view))
    titlecase(str)
  end

  required_entries(:: ImageView) = []
  on_init!(images, :: ImageView, ctx :: Context) = nothing
  updater(:: ImageView, ctx :: Context) = nothing
  on_update!(images, :: ImageView, ctx :: Context, sel) = nothing

  Makie.optionlabel(T :: Type{<: ImageView}) = string(name(T))
  Makie.optionvalue(T :: Type{<: ImageView}) = T
  

  """
  Image view that initializes images to zero and never updates. Requires
  no context entries.
  """
  struct EmptyView <: ImageView end
  EmptyView(ctx :: Context) = EmptyView()

  name(:: Type{EmptyView}) = :empty

  function on_init!(images, :: EmptyView, ctx :: Context)
    for i in 1:length(images)
      images[i][] .= 0
    end
  end


  """
  Image view that initializes images to the problem targets and never updates.
  Requires no context entries.
  """
  struct TargetView <: ImageView end
  TargetView(ctx :: Context) = TargetView()

  name(:: Type{TargetView}) = :target

  function on_init!(images, :: TargetView, ctx :: Context)
    problem = ctx[:problem][]
    for index in 1:length(problem)
      target = MuSink.target(problem, index)
      images[index][] = dropdims(target, dims = 3)
    end
  end


  """
  Image view that initializes images to the workspace marginals and never updates.
  Requires no context entries.
  """
  struct MarginalView <: ImageView end
  MarginalView(ctx :: Context) = MarginalView()

  name(:: Type{MarginalView}) = :marginal
  updater(:: MarginalView, ctx :: Context) = ctx[:update]

  function on_update!(images, :: MarginalView, ctx :: Context, sel)
    problem = ctx[:problem]
    imgs = query_workspace(ctx, :view_marginal) do ws
      map(sel) do index
        img = MuSink.marginal(ws, index)
        convert(Array, img)
      end
    end
    for (i, index) in enumerate(sel)
      images[index][] = dropdims(imgs[i], dims = 3)
    end
  end


  """
  Image view that initializes images to the difference between workspace
  marginals and problem targets and keeps them updated. Requires no context
  entries.
  """
  struct DifferenceView <: ImageView end
  DifferenceView(ctx :: Context) = DifferenceView()

  name(:: Type{DifferenceView}) = :difference
  updater(:: DifferenceView, ctx :: Context) = ctx[:update]

  function on_update!(images, :: DifferenceView, ctx :: Context, sel)
    problem = ctx[:problem][]
    imgs = query_workspace(ctx, :view_difference) do ws
      map(sel) do index
        img = MuSink.marginal(ws, index)
        convert(Array, img)
      end
    end
    for (i, index) in enumerate(sel)
      target = MuSink.target(problem, index)
      images[index][] = dropdims(target .- imgs[i], dims = 3)
    end
  end


  """
  Image view that highlights areas where the local transport cost is high.
  """
  struct CostView <: ImageView
    red :: MuSink.Reduction
  end

  function CostView(ctx :: Context)
    ws = ctx[:workspace][]
    red = MuSink.Reduction(ws, (_,_,c) -> c)
    CostView(red)
  end

  name(:: Type{CostView}) = :cost
  required_entries(cv :: CostView) = [:update_transport]
  updater(:: CostView, ctx :: Context) = ctx[:update_transport]

  function on_update!(images, cv :: CostView, ctx :: Context, sel)
    problem = ctx[:problem][]
    imgs = query_workspace(ctx, :view_cost) do ws
      map(sel) do index
        node = Tree.descendant(ws.root, index)
        img = mean(Tree.neighbors(node)) do neighbor
          MuSink.reduce(cv.red, ws, node, neighbor)
        end
        convert(Array, img)
      end
    end
    for (i, index) in enumerate(sel)
      target = MuSink.target(problem, index)
      images[index][] = dropdims(imgs[i], dims = 3)
    end
  end

  # """
  #     ImageView(name)

  # Construct an image view with name `name`.
  # """
  # function ImageView(sym :: Symbol, args...)
  #   for V in subtypes(ImageView)
  #     if name(V) == sym
  #       return V(args...)
  #     end
  #   end
  #   raise(ArgumentError("no image view with name $sym registered"))
  # end

  # function ImageView(str :: String, args...)
  #   sym = lowercase(str) |> Symbol
  #   ImageView(sym, args...)
  # end

end # module Views
