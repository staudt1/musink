

module Widgets

  using Statistics, Random, Printf
  import CUDA
  using GLMakie
  using Observables

  import MuSink
  import MuSink: Problem, Workspace, Tree

  import ..Views
  import ..Interactions
  import ..Context, ..query_workspace

  """
  Building blocks for a graphical user interface to visualize UMOT
  problems via `Makie`.
  """
  abstract type Widget end

  function context_require(ctx, args :: Symbol ...)
    @assert all(k -> haskey(ctx, k), args)
  end

  function context_provide(ctx, args :: Symbol ...)
    @assert !any(k -> haskey(ctx, k), args)
  end

  function boxed_layout( box_layout,
                         title,
                         tellwidth = true;
                         add_to_head = nothing,
                         blocks = nothing )

    layout = box_layout[1,1] = GridLayout(alignmode = Inside())
    head = layout[1,1] = GridLayout(; halign = :left, tellwidth)
    body = layout[2,1] = GridLayout(halign = :left, default_rowgap = 7)

    box = Box(box_layout[1,1], color = :transparent, strokewidth = 0.0)
    lb = Label(head[1,1], title, halign = :left, fontsize = 14)

    if !isnothing(blocks)
      push!(blocks, box)
      push!(blocks, lb)
    end

    if isnothing(add_to_head)
      body
    else
      layout = head[1,2] = GridLayout(halign = :center, tellwidth = tellwidth)
      body, add_to_head(layout)
    end
  end

  function boxed_layout_reset(box_layout, title, tellwidth = true)
    add_to_head = layout -> begin
      Button(
        layout[1,1],
        label = "Reset",
        halign = :right,
        tellheight = false,
        tellwidth = tellwidth
      )
    end
    boxed_layout(box_layout, title; add_to_head)
  end

  function boxed_layout_horizontal(box_layout, title; blocks = nothing)
    layout = box_layout[1,1] = GridLayout(alignmode = Inside())
    head = layout[1,1] = GridLayout(tellheigth = false)
    body = layout[1,2] = GridLayout()

    box = Box(box_layout[1,1], color = :transparent, strokewidth = 0.0)
    lb = Label(
      head[1,1],
      title,
      fontsize = 14,
      rotation = 0.5pi,
      tellheight = false,
      valign = :center
    )

    if !isnothing(blocks)
      push!(blocks, box)
      push!(blocks, lb)
    end

    body
  end

  function fill_vertical_space(layout)
    ax = Axis(layout[end+1, 1:end])
    hidedecorations!(ax)
    hidespines!(ax)
  end

  function block_delete!(block)
    delete!(block)
  end

  function block_delete!(ax :: Axis)
    for key in keys(Makie.interactions(ax))
      deregister_interaction!(ax, key)
    end
    delete!(ax)
  end

  function block_hide!(block)
    block.blockscene.visible[] = false
  end

  function block_unhide!(block)
    block.blockscene.visible[] = true
  end

  # TODO: Makie bug...
  function block_hide!(block :: Textbox)
    block.blockscene.visible[] = false
    block.width[] = 0
  end

  function block_unhide!(block :: Textbox)
    block.blockscene.visible[] = true
    block.width[] = 40 # TODO: store the width somewhere?
  end

  function deregister_interactions!(ax :: Axis)
    for key in keys(ax.interactions)
      deregister_interaction!(ax, key)
    end
  end

  function deactivate_interactions!(ax :: Axis)
    for key in keys(ax.interactions)
      deactivate_interaction!(ax, key)
    end
  end

  function activate_interactions!(ax :: Axis)
    for key in keys(ax.interactions)
      activate_interaction!(ax, key)
    end
  end

  function hide_axis_spine!(ax :: Axis)
    ax.rightspinevisible[] = false
    ax.leftspinevisible[] = false
    ax.topspinevisible[] = false
    ax.bottomspinevisible[] = false
  end

  include("widgets/problem-overview.jl")
  include("widgets/area-view-select.jl")
  include("widgets/workspace-options.jl")
  include("widgets/workspace-status.jl")
  include("widgets/workspace-parameters.jl")
  include("widgets/transport-options.jl")
  include("widgets/node-row.jl")
  include("widgets/image-row.jl")
  include("widgets/target-row.jl")
  include("widgets/marginal-row.jl")
  include("widgets/difference-row.jl")

end
