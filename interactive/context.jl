
"""
Type that implements communication between different widgets and communication
between the widgets and the stepping engine of `MuSink`. It is always
guaranteed to hold certain observables that can be accessed via the keys

  * :problem
  * :workspace
  * :update
  * :update_interval
  * :node_names
  * :blocking_queries
  * :running
  * :message
  * :exit

These observables are necessary and sufficient to run (and exit) the
interactive main loop (see `init`), but other observables may be attached
to the context by `Widget`s (see the respective documentation). The `:update`
observable is notified after each step of the main loop, while the `:running`
observable can be set externally to start / stop the computational loop that
modifies the workspace. Setting the `:exit` observable to true makes the main
loop return.

While updating the `workspace` observable with a new, unrelated workspace
is supported, accessing the workspace directly after calling `init` is not
advised, since it may be operated on in a diffent thread or may generally be in
a non-coherent state when accessed (i.e., in the middle of a step).
Still, the workspace can be interacted with safely via the `query_workspace`
convenience method. This method implements communication with the workspace via
channels and should be safe to use under any circumstances.
"""
struct Context
  observables :: Dict{Symbol, Observable}
  query_workspace :: Channel{Tuple{Function, Symbol, Channel}}
end

Base.getindex(ctx :: Context, args...) = getindex(ctx.observables, args...)
Base.setindex!(ctx :: Context, args...) = setindex!(ctx.observables, args...)
Base.haskey(ctx :: Context, args...) = haskey(ctx.observables, args...)
Base.keys(ctx :: Context) = keys(ctx.observables)

function Context(
                  problem :: Observable{Problem};
                  node_names = Observable{Any}(nothing),
                  query_buffer :: Int = 100,
                )
  # Derive a default workspace from the problem. The workspace options and
  # parameters should be updated by the interaction provider (e.g., the gui)
  workspace = @lift Workspace(Array{Float32}, $problem; eps = 1, rho = Inf)
  # We have to explicitly delete type parameter information, since the type
  # parameters of the workspace (e.g., Array{Float64} or CuArray{Float32}) may
  # change with an update
  workspace = convert(Observable{Workspace}, workspace)

  # Whenever the workspace is updated, a new channel 'query' is put in the
  # query_workspace channel below. This channel 'query' can safely be used to
  # communicate queries to the background worker
  query_workspace = Channel{Tuple{Function, Symbol, Channel}}(query_buffer)

  # These default observables provide everything needed to initialize the
  # interactive context. Further observables, for example providing inter-
  # widget communication, can be added by interaction providers
  observables =
    Dict{Symbol, Observable}(
      :problem => problem,
      :node_names => node_names,
      :workspace => workspace,
      :update => Observable{Nothing}(nothing),
      :update_interval => Observable{Int}(10),
      :blocking_queries => Observable{Vector{Symbol}}([]),
      :running => Observable{Bool}(false),
      :message => Observable{String}(""),
      :exit => Observable{Bool}(false),
    )

  Context(observables, query_workspace)
end

"""
    register!(ctx, figure)

Register a figure. This adds the observables
  * `:figure_area`
to the context.
"""
function register!(ctx, fig :: Figure)
  ctx[:figure_area] = fig.scene.px_area
end

"""
    query_workspace(f, ctx [, name])

Auxiliary method to safely interact with the live workspace of `ctx` even while
it is being operated on. The function `f` is applied to `ctx[:workspace]`. The
optional argument `name` is a symbol with information about the query (useful
mainly for debugging).
"""
function query_workspace(f :: Function, ctx :: Context, name = nameof(f))
  result = Channel{Any}(1)
  put!(ctx.query_workspace, (f, name, result))
  take!(result)
end


function worker(workspace, query, running, message, update, interval, block)
  @debug "Context: worker: starting computational main loop"

  # After modifying the workspace, we have to synchronize the CUDA stream if
  # the workspace operates on a GPU. Without synchronization, NaNs occured
  lk = ReentrantLock()
  safely = f -> lock(lk) do
    f()
    CUDA.functional() && CUDA.synchronize()
  end

  # Document which queries have been answered since the last update was submitted
  answered = Channel{Symbol}(10000)

  respond = q -> begin
    try
      f, name, ch = q
      @debug "Context: worker: responding to query :$name"
      safely() do
        ws = fetch(workspace)
        put!(ch, f(ws))
      end
      put!(answered, name)
    catch err
      @error "Context: worker: unexpected error: $err"
      put!(message, string(err))
    end
  end

  # Answer the queries that have accumulated asynchronously
  # This makes responding possible even if the workspace is not running.
  # If the workspace is replaced, the channel query is closed.
  # All outstanding queries are answered by accessing the old workspace.
  task = @async begin
    @debug "Context: worker: starting query task"
    try while true
        respond(take!(query))
      end
    catch err
      if err isa InvalidStateException
        close(answered)
        @debug "Context: worker: query task received exit signal"
      else
        @error "Context: worker: unexpected error: $err"
        put!(message, string(err))
      end
    end
  end

  # Run MuSink steps if running = true
  while fetch(running)
    steps = fetch(interval)
    @debug "Context: worker: running $steps steps"
    try safely() do
        ws = fetch(workspace)
        MuSink.steps!(ws, fetch(interval))
      end
    catch err
      @error err
      put!(message, string(err))
      break
    end

    # Empty the channel of answered queries so that it can be freshly populated
    # after the update has been propagated
    while isready(answered)
      take!(answered)
    end
    # Inform the ctx[:update] observable that a step has been performed
    put!(update, nothing)

    # Some interaction agents may wish to react on an update and have their
    # queries answered before the next steps are conducted.
    # In order to allow for this, the block channel can be populated with a
    # vector of symbols (via the :blocking_queries observable), which denote
    # the names of prioritized queries that have to be responded to first
    # before the iterations may continue.

    blocking = Set(fetch(block))
    while !isempty(blocking)
      try
        name = take!(answered)
        delete!(blocking, name) # remove name of this query from the list
      catch err
        if err isa InvalidStateException
          @debug "Context: worker: channel blocking_queries was closed"
        else
          @error err
        end
        break
      end
    end

    # Be friendly and let other tasks also do some work
    yield()
  end

  close(query)
  wait(task)
  @debug "Context: worker: exiting computational main loop"
end


  # Error handling for translating between channels and observables
function loop(f)
  try
    while true f() end
  catch err
    if err isa InvalidStateException
      @debug "Context: loop: exit from internal loop"
    else
      @debug "Context: loop: unexpected exception in internal loop: $err"
    end
  end
end


"""
    init(ctx; threaded = false)

Run the main loop of the interactive context `ctx`. Returns immediately. If
`threaded` is true, the computational main loop is spawned into its own thread.
"""
function init(ctx :: Context; threaded = false)
  @debug "Context: init: initializing context"

  ## Channels that bring information to the worker

  # The current workspace
  workspace = Channel{Workspace}(1)
  # The update interval
  interval = Channel{Int}(1)
  # Should update-steps be performed. Keep empty to pause the worker
  running = Channel{Bool}(1)
  # Vector of blocking symbols
  block = Channel{Vector{Symbol}}(1)

  ## Channels that collect information from the worker

  # An update interval has been finished
  update = Channel{Nothing}(1)
  # A message from the worker
  message = Channel{String}(1)

  task_update =
    @async loop() do
      take!(update)
      notify(ctx[:update])
    end

  task_message =
    @async loop() do
      msg = take!(message)
      ctx[:message][] = msg
    end

  obs_workspace =
    on(ctx[:workspace], update = true) do ws
      @debug "Context: channeling update for observable :workspace"
      isready(workspace) && take!(workspace)
      put!(workspace, ws)
    end

  obs_running =
    on(ctx[:running], update = true) do run
      @debug "Context: channeling update for observable :running"
      isready(running) && take!(running)
      run && put!(running, true)
    end

  obs_interval =
    on(ctx[:update_interval], update = true) do value
      @debug "Context: channeling update for observable :update_interval"
      isready(interval) && take!(interval)
      put!(interval, value)
    end

  obs_block =
    on(ctx[:blocking_queries], update = true) do value
      @debug "Context: channeling update for observable :blocking_queries"
      isready(block) && take!(block)
      put!(block, value)
    end

  if threaded
    task_worker =
      Threads.@spawn try
       worker(
        workspace,
        ctx.query_workspace,
        running,
        message,
        update,
        interval,
        block
      )
      catch err
        @error err
      end
  else
    task_worker =
      @async worker(
        workspace,
        ctx.query_workspace,
        running,
        message,
        update,
        interval,
        block
      )
  end

  obs_exit = nothing
  obs_exit = on(ctx[:exit]) do exit
    if exit
      @debug "Context: init: received exit signal"

      # remove observer connections
      off(obs_workspace)
      off(obs_running)
      off(obs_interval)
      off(obs_block)
      !isnothing(obs_exit) && off(obs_exit)

      # stop the worker
      put!(running, false)
      close(ctx.query_workspace)
      wait(task_worker)

      # close channels
      close(workspace)
      close(interval)
      close(running)
      close(block)

      close(update)
      wait(task_update)

      close(message)
      wait(task_message)

      @debug "Context: init: exit cleanup complete"
    end
  end

  @info "Context: interactive context initialized"
  nothing
end
